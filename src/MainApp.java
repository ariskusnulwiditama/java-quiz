import app.App;

import java.util.HashMap;
import java.util.Map;

public class MainApp {
    public static void main(String[] args) throws Exception {
        Map<Double, Integer> voucher = new HashMap<Double, Integer>();
        App application = new App();
        voucher.put(10000D, 100);
        voucher.put(25000D, 200);
        voucher.put(50000D, 400);
        voucher.put(100000D, 800);

        System.out.println("1. voucher with the highest point: ");
        application.highestPoint(voucher).forEach((key, value) -> {
            System.out.println(key + " = " + value + "p");
        });

        System.out.println("2. voucher remain with the highest point if point is 1000p: ");
        System.out.println(application.redeemWithTheHighestPoint(1000, voucher) + "point");

        System.out.println("3. voucher which get from point remain");
        application.redeemWithHighestPointPriority(1150, voucher).forEach((key, value) -> {
            System.out.print(key + ":" );
            System.out.println(value);
        });
    }
}
