package app;

import java.util.*;
import java.util.stream.Collectors;

public class App {
    public Map<String, Integer> highestPoint(Map<Double, Integer> voucherMap) {
        Map<String, Integer> highestPoint = new HashMap<>();

        int maxValue = (Collections.max(voucherMap.values()));
        String key = null;
        for(Map.Entry<Double, Integer> entry : voucherMap.entrySet()) {
            if(entry.getValue().equals(maxValue)) {
                key = "Rp. "+ entry.getKey().intValue();
                highestPoint.put(key, maxValue);
            }
        }
        return highestPoint;
    }

    public Map<String, Integer> redeemWithHighestPointPriority(int currentPoint, Map<Double, Integer> voucherMap) {
        Map<String, Integer> redeemWithTheHighestPoint = new LinkedHashMap<>();
        List<Integer> highestPoint = voucherMap.values().stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());

        List<Double> highestKey = voucherMap.keySet().stream().sorted(Collections.reverseOrder()).collect(Collectors.toList());

        for(int i=0; i<highestKey.size(); i++) {
            if(highestKey.get(i) <= currentPoint) {
                currentPoint = currentPoint - highestPoint.get(i);
                if(Boolean.FALSE.equals(redeemWithTheHighestPoint.containsKey("Rp. " + highestKey.get(i).intValue()+" "))) {
                    redeemWithTheHighestPoint.put("Rp. " + highestKey.get(i).intValue()+ " ", 1);
                }else{
                    redeemWithTheHighestPoint.replace("Rp. " + highestKey.get(i).intValue() + " ", redeemWithTheHighestPoint.get("Rp. " + highestKey.get(i).intValue() + " ")+1);
                }
            }else{
                i++;
            }
        }

        redeemWithTheHighestPoint.put("current point ", currentPoint);
        return redeemWithTheHighestPoint;
    }

    public int redeemWithTheHighestPoint(int currentPoint, Map<Double, Integer> redeemPoints) {
        int highestPoint = (Collections.max(redeemPoints.values()));
        int pointLeft = currentPoint - highestPoint;

        return pointLeft;
    }
}
